<?php

$last_restart = 0;

for (; ;) {
    _check();
    sleep(60);
}


function _check()
{
    global $last_restart;
    $now = time();

    if ($now - $last_restart > 3600 * 3) {
        exec("killall node");
        $last_restart = $now;
        _print("nodes restarted");
    }

    $cmd = "ps ax | grep -e node | grep -v grep";
    exec($cmd, $out);
    $nodes = array(
        array("title" => "_st_bot", "present" => false),
        array("title" => "_dota_bot", "present" => false),
        array("title" => "_tf_bot", "present" => false),
        array("title" => "_cs_bot", "present" => false),
        array("title" => "fiery_soul_of_the_slayer", "present" => false)
//        array("title" => "inscribed_fiery_soul_of_the_slayer", "present" => false)
    );

    if (count($out) > 0) {
        print_r($out);
        foreach ($out as $val) {
            foreach ($nodes as $k => $v) {
                if (preg_match("/" . $v["title"] . "/", $val)) {
                    $nodes[$k]["present"] = true;
                    break;
                }
            }
        }
    }

    foreach ($nodes as $value) {
        if (!$value["present"]) {
            _print($value["title"] . " is absent today ");
            _print($value["title"] . " - restarting ... ");
            exec("nohup node bots/" . $value["title"] . ".js >> logs/bots.log 2>&1&");
        }
    }
}

function _print($msg)
{
    echo date("Y-m-d H:i:s") . " : " . $msg . "\n";
}

function get_process_ids($str, $data)
{
    $res = array();
    foreach ($data as $value) {
        if (preg_match("/" . $str . "/", $value) && preg_match("/(\d+)\s/", $value, $match)) {
            if (isset($match[1])) {
                $id = trim($match[1]);
                if (is_numeric($id))
                    $res[] = trim($id);
            }
        }
    }
    return $res;
}
