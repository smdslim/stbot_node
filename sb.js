var _utils = require("_utils"),
    Bot = require("bot"),
    bot = new Bot(_utils.gamesData.DOTA.id),
    sellBot = Object.create(bot),
    http = require("http"),
    async = require("async"),
    cheerio = require("cheerio");


sellBot.sellInterval = 10000;

sellBot.percent = 23;

sellBot.sellingInventories = [ _utils.games.TF, _utils.games.CS, _utils.games.STEAM, _utils.games.DOTA ];

sellBot.sellQueue = {};

sellBot.sellConfig = {};

sellBot.begin = function (callback) {
    var _this = this,
        lots = {},
        lot,
        tempLots = {},
        gameIterator,
        lotIterator,
        game,
        itemName,
        sellGameIterator,
        sellLotIterator,
        sellOrderLots = [],
        sellOrderGames = [];

    _this.sellConfig = _utils.getConfig().sellConfig;

    gameIterator = function (i1) {
        if (_this.sellingInventories.length > 0 && _this.sellingInventories[i1] != undefined) {
            game = _this.sellingInventories[i1];
            _this.getLotsToSell(game, function (err, res) {
                if (!err) {
                    lotIterator = function (i2) {
                        if (res.classes.length > 0 && res.classes[i2] != undefined) {
                            itemName = res.classes[i2];
                            _this.getAveragePrice(_utils.gamesData[game].id, itemName, function (err, avgPrice) {
                                if (!err) {
                                    tempLots = {};
                                    _this.appendPriceToLot(res.lots, itemName, _this.calculateEndPrice(avgPrice));
                                    for (var key in res.lots) {
                                        if (res.lots.hasOwnProperty(key) && lots[key] == undefined) {
                                            tempLots[key] = res.lots[key];
                                            tempLots[key].gameID = _utils.gamesData[game].id;
                                            if(_utils.gamesData[game] === undefined){
                                                _utils.logEvent(" --- UNDEFINED GAME, WHY IS THIS HAPPENING? ---", game);
                                            }
                                        }
                                    }
                                    lots[_utils.gamesData[game].id] = tempLots;
                                }
                                lotIterator(++i2);
                            })
                        } else {
                            gameIterator(++i1);
                        }
                    }
                    lotIterator(0);
                } else {
                    gameIterator(++i1);
                }
            })
        } else {
            if (_utils.countElements(lots) > 0) {
                sellOrderGames = Object.keys(lots);
                sellGameIterator = function (i3) {
                    game = sellOrderGames[i3];
                    if (game != undefined && lots[game] != undefined) {
//                        console.log("Selling items from game: " + game);
                        sellOrderLots = Object.keys(lots[game]);
                        sellLotIterator = function (i4) {
                            lot = sellOrderLots[i4];
                            if (lot !== undefined && lots[game][lot] !== undefined && isNaN(lots[game][lot].sellPrice) === false) {
                                _this.sellItem(lot, lots[game][lot].sellPrice, lots[game][lot].cid, lots[game][lot].name, game, function () {
                                    sellLotIterator(++i4);
                                });
                            } else {
                                sellGameIterator(++i3);
                            }
                        }
                        sellLotIterator(0);
                    } else {
                        callback(null, "Go Next");
                    }
                }
                sellGameIterator(0);
            } else {
                // _utils.logEvent("Go for next iteration");
                callback(null, "Go Next");
            }
        }
    }

    gameIterator(0);

};

sellBot.sellItem = function (id, price, cid, itemName, gameID, callback) {
    var _this = this;
    _this.busy = true;

    var script = "var xhr = new XMLHttpRequest(); xhr.open('POST', 'https://steamcommunity.com/market/sellitem', true); xhr.withCredentials = true; xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8'); xhr.send('sessionid=" + _this.sessionData.sessionid + "&appid=" + gameID + "&contextid=" + cid + "&assetid=" + id + "&amount=1&price=" + price + "');"    // console.log(script);

    this.driver.executeScript(script).then(function () {
        setTimeout(function () {
            _utils.logEvent("item exposed: '" + itemName + "'; price: " + price);
            _this.busy = false;
            // delete _this.sellQueue[id];
            // _this.manageQueue(callback);
            callback(null);
        }, 10000)
    });
}

sellBot.manageQueue = function (callback) {
    var el;
    _utils.logEvent("Managing queue...");
    if (_utils.countElements(this.sellQueue) > 0) {
        el = this.getElementFromQueue();
        _utils.logEvent("Will sell next: " + el.itemName);
        this.sellItem(el.id, el.price, el.cid, el.itemName, el.gameID, callback);
    } else {
        _utils.logEvent("No items in queue...");
        callback(null, "Go next");
    }
};

sellBot.getElementFromQueue = function () {
    for (var el in this.sellQueue) {
        if (this.sellQueue.hasOwnProperty(el)) {
            return this.sellQueue[el];
        }
    }
    return {};
};

sellBot.addToQueue = function (id, price, cid, itemName, gameID, callback) {
    if (this.busy) {
        _utils.logEvent("Bot is busy, adding item to queue");
        this.sellQueue[id] = {id: id, price: price, cid: cid, itemName: itemName, gameID: gameID};
    } else {
        _utils.logEvent("Bot is free, selling now");
        this.sellItem(id, price, cid, itemName, gameID, callback);
    }
};

sellBot.startSelling = function (gameID, lots, callback) {
    for (var id in lots) {
        if (lots.hasOwnProperty(id)) {
            this.addToQueue(id, lots[id].sellPrice, lots[id].cid, lots[id].name, gameID, callback);
        }
    }
};

sellBot.calculateEndPrice = function (avgPrice) {
    var price;

    price = parseFloat(avgPrice) * 1000 / 10;
    price = parseInt(price - (Math.round(price * this.percent / 100)));

    return price;
};

sellBot.appendPriceToLot = function (lots, itemName, price) {
    for (var id in lots) {
        if (lots.hasOwnProperty(id) && lots[id].name != undefined) {
            if (lots[id].name == itemName) {
                lots[id].sellPrice = price;
            }
        }
    }
}

sellBot.getCookieString = function () {
    var str = ""
    for (var key in this.sessionData) {
        str += key + "=" + this.sessionData[key] + "; "
    }
    return str;
};

sellBot.getAveragePrice = function (gameID, item, callback) {
    var _this = this;
    var options = {
        hostname: _this.steamUrl,
        port: 80,
        path: "/market/listings/" + gameID + "/" + encodeURIComponent(item) + "/render/?query=&start=0&count=8",
        method: 'GET',
        headers: {
            Cookie: this.getCookieString()
        }
    };
    var request = http.request(options, function (res) {
        var data = "",
            prices, averagePrice, suitableID, url;
        res.on("data", function (chunk) {
            data += chunk.toString();
        });
        res.on("end", function () {
            try {
                data = unescape(JSON.parse(data).results_html);
            } catch (e) {
                callback(e.message, "ready for next search: " + item);
                return 0;
            }
            prices = _this.parsePage(data, item);
            averagePrice = _this.calculateAvgPrice(prices);
            callback(null, averagePrice);
        })
    });
    request.on("close", function (e) {
        try {
            callback("error loading page: " + options.hostname + options.path);
        } catch (e) {
            _utils.logEvent(e.message);
        }

    });
    request.on('error', function (e) {
        try {
            callback(e.message);
        } catch (e) {
            _utils.logEvent(e.message);
        }

    });
    request.end();
};

//"http://steamcommunity.com/id/smdslim/inventory/json/570/2/"
sellBot.getLotsToSell = function (game, callback) {
    var _this = this,
        classes,
        lots,
        tempClasses = [],
        tempClass;

    var request = http.get(_utils.gamesData[game].url, function (res) {
        var data = "", lotNames = [];

        res.on("data", function (chunk) {
            data += chunk.toString();
        })

        res.on("close", function () {
            _utils.logEvent("closed");
            callback("Connection closed");
        })

        res.on("end", function () {

            try {
                classes = JSON.parse(data).rgDescriptions;
                lots = JSON.parse(data).rgInventory;

                if (typeof lots == "object" && typeof classes == "object") {
                    _this.clearUnmarketableItems(lots, classes);

                    for (var id in lots) {
                        if (lots.hasOwnProperty(id)) {
                            tempClass = _this.getClassTitle(lots[id].classid, classes);
                            if (tempClass !== false) {
                                lots[id].name = tempClass;
                                lots[id].cid = _utils.gamesData[game].cid;
                                tempClasses.push(tempClass);
                            } else {
//                                _utils.logEvent("Could not get class name of classid / or instance id = 0; classid: " + lots[id].classid);
//                                delete lots[id];
                            }
                        }
                    }
                }

            } catch (e) {
                callback("Error parsing classes/lots; " + e.message);
            }

            callback(null, {lots: lots, classes: tempClasses});

        })
    })

    request.on('error', function (e) {
        callback(e.message);
    });

}

sellBot.clearUnmarketableItems = function (lots, classes) {
    var _this = this;
    var unmarketable = [];
    var usellableHeroes = ["npc_dota_hero_windrunner", "npc_dota_hero_lina", "npc_dota_hero_pudge"];

    for (var key in classes) {
        if (
            (classes.hasOwnProperty(key) && classes[key].marketable !== undefined && classes[key].marketable == 0)
                ||
                classes[key].market_hash_name == "Treasure Key") {

            unmarketable.push(classes[key].classid);
        } else {

            if (classes[key].tags[4] != undefined) {

                if (classes[key].market_name !== undefined && _this.sellConfig.forseSellItemNames.indexOf(classes[key].market_name) != -1) {
                    continue;
                }

                if (classes[key].market_name !== undefined && _this.sellConfig.notToSellItemNames.indexOf(classes[key].market_name) != -1) {
                    unmarketable.push(classes[key].classid);
                }

                if (usellableHeroes.indexOf(classes[key].tags[4].internal_name) != -1) {
                    unmarketable.push(classes[key].classid);
                }
            }
        }
    }

    for (var id in lots) {
        if (lots.hasOwnProperty(id) && (unmarketable.indexOf(lots[id].classid) != -1 && _this.sellConfig.forseSellIDs.indexOf(id) == -1) || _this.sellConfig.notToSellItemIDs.indexOf(id) != -1) {
            delete lots[id];
        }
    }

};

sellBot.getClassTitle = function (classID, classes) {
    for (var key in classes) {
        if (classes.hasOwnProperty(key) && classes[key].classid != undefined) {
            if (classes[key].classid == classID && (classes[key].market_hash_name != undefined || classes[key].market_name != undefined)) {
                return classes[key].market_hash_name === undefined ? classes[key].market_name : classes[key].market_hash_name;
            }
        }
    }
    return false;
}

sellBot.startReExposing = function (callback) {
    var _this = this,
        $,
        _class,
        itemsToReExpose = [],
        sellID,
        currentSellID,
        itemID,
        script,
        itemsToCheck = [],
        itemsURLs = {},
//        conf,
        url,
        elementPosition,
        findElements,
        exposeElements;

    _utils.logEvent("Scan for reexpose items started ... ");
    this.navigation.refresh().then(function () {
//        conf = _utils.getConfig();
        _this.driver.getPageSource().then(function (html) {
            $ = cheerio.load(html);
            $(".market_content_block.my_listing_section.market_home_listing_table .market_recent_listing_row").each(function () {
                _class = $(this).attr("class");
                try {
                    sellID = /\d+/.exec(_class)[0];
                    itemID = parseInt(/.+'(\d+)'\)$/.exec($(this).find(".item_market_action_button.item_market_action_button_edit").attr("href"))[1]);
                } catch (e) {
                    _utils.logEvent("Could not parse sellID/itemID. From class " + _class + "; " + e.message);
                }
                if (_this.sellConfig.notToReexposeItemIDs.indexOf(itemID) == -1) {
                    itemsToCheck.push(sellID);
                    itemsURLs[sellID] = $(this).find(".market_listing_item_name_link").attr("href");
                }
            })
            if (itemsToCheck.length > 0) {
                _utils.logEvent("Found " + itemsToCheck.length + " items to check");
                findElements = function (j) {
                    sellID = itemsToCheck[j];
                    url = itemsURLs[sellID];
                    if (sellID != undefined && url != undefined) {
                        _this.driver.get(url).then(function () {
                            _this.driver.getPageSource().then(function (html) {
                                $ = cheerio.load(html);
                                elementPosition = 0;
                                $(".market_listing_row.market_recent_listing_row").each(function () {
                                    elementPosition++;
                                    _class = $(this).attr("class");
                                    try {
                                        currentSellID = /\d+/.exec(_class)[0];
                                    } catch (e) {
                                        _utils.logEvent("Could not parse currentSellID")
                                    }
                                    if (currentSellID == sellID) {
                                        return false;
                                    }
                                })
                                if (elementPosition >= 3) {
                                    _utils.logEvent("Id: " + sellID + "; pos: " + elementPosition + "; added to reexpose list");
                                    itemsToReExpose.push(sellID);
                                }
                                findElements(++j);
                            })
                        })
                    } else {
                        if (itemsToReExpose.length > 0) {
                            _this.driver.get("http://steamcommunity.com/market").then(function () {
                                _utils.logEvent("Found " + itemsToReExpose.length + " to reexpose ...");
                                exposeElements = function (k) {
                                    sellID = itemsToReExpose[k];
                                    if (sellID != undefined) {
                                        script = _utils.reexposeScript(sellID, "sessionid=" + _this.sessionData.sessionid);
                                        _this.driver.executeScript(script).then(function () {
                                            setTimeout(function () {
                                                exposeElements(++k);
                                            }, 2500);
                                        })
                                    } else {
                                        callback(null, "Done");
                                    }
                                }
                                exposeElements(0);
                            })
                        } else {
                            callback(null, "Done");
                        }
                    }
                }
                findElements(0);
            } else {
                callback(null, "Done");
            }
        });
    });
}

module.exports = sellBot;



