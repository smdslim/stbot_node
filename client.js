var http = require("http"),
    fs = require("fs"),
    Bot = require("bot"),
    _utils = require("_utils");

bot = new Bot(_utils.gamesData.DOTA.id);

bot.init(function () {
    _utils.logEvent("Bot has been initiated");
    setInterval(function () {
        getItems(function (err, items) {
            if (!err) {
                try {
                    if (typeof items == "object" && !isEmpty(items)) {
                        for (var id in items) {
                            _utils.logEvent(items[id].url + "; id: " + id + "; itemName: " + items[id].itemName + "; price: " + items[id].price);
                            bot.addToQueue(items[id].url, id, items[id].itemName);
                        }
                    }
                } catch (e) {
                    console.log(e.message);
                }
            } else {
                console.log(err);
            }
        });
    }, 1000)
});


function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key)) {
            return false;
        }
    }
    return true;
}

function getItems(callback) {
    var result = {}, str, fileName;
    fs.readdir("./ids", function (err, files) {
        if (!err) {
            for (var i = 0; i < files.length; i++) {
                fileName = files[i];
                if (fileName !== ".gitkeep") {
                    try {
                        str = fs.readFileSync("./ids/" + fileName).toString();
                        result[fileName] = JSON.parse(str);
                        fs.unlinkSync("./ids/" + fileName);
                    } catch (e) {
                        _utils.logEvent("Could not read/parse lot file, id(" + fileName + "): " + e.message + "; Content: " + str)
                    }
                }
            }
            callback(null, result);
        } else {
            _utils.logEvent("Could not read lot directory: " + err)
        }
    })
}


