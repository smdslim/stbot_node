var http = require('http'),
    url = require("url"),
    fs = require("fs");


var server = new http.Server(function (req, res) {

    var queryData = url.parse(req.url, true).query;
    var result = {};

    if (queryData.scanFiles != undefined) {
        fs.readdir("./ids", function (err, files) {
            if (!err) {
                for (var i = 0; i < files.length; i++) {
                    result[files[i]] = fs.readFileSync("./ids/" + files[i]).toString();
                    fs.unlinkSync("./ids/" + files[i]);
                }
                res.end(JSON.stringify(result));
            } else {
                console.log(err);
            }
        })
    } else if (queryData.deleteFiles != undefined && queryData.files != undefined) {
        var files = JSON.parse(queryData.files);
        if (typeof files == "object" && files.length > 0) {
            try{
                for (var i = 0; i < files.length; i++) {
                    fs.unlinkSync("./ids/" + files[i]);
                }
                result.success = true;
            }catch(e){
                result.success = false;
                result.msg = e.message;
            }
        }
        res.end(JSON.stringify(result));
    }
});


server.listen(3001);