<?php

if (isset($argv[1])) {
    if ($argv[1] == "-start") {
        if ($argv[2] == 1) {
            echo "executing first monitoring script ...\n";
            exec("nohup php monitoring/monitoring_service.php >> logs/monitoring_01.log 2>&1&");
        } else if ($argv[2] == 2) {
            echo "executing second monitoring script ...\n";
            exec("nohup php monitoring/monitoring_service_02.php >> logs/monitoring_02.log 2>&1&");
        }
    } else if ($argv[1] == "-kill") {
        if ($argv[2] == "all") {
            echo "executing killing all script ...\n";
            exec("killall php");
            exec("killall node");
            exec("killall firefox");
        } else if ($argv[2] == "node") {
            echo "executing kill node script ...\n";
            exec("killall node");
        } else if ($argv[2] == "php") {
            echo "executing kill php script ...\n";
            exec("killall php");
        } else if ($argv[2] == "firefox") {
            echo "executing firefox ...\n";
            exec("killall firefox");
        }
    }

    echo "Done!\n";
} else {
    echo "Provide needed parameters\n";
}
