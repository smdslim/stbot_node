var Bot = require("bot"),
    _utils = require("_utils");

bot = new Bot(_utils.gamesData.DOTA.id);
bot.options.edgePrice = 300;

scan("Fiery Soul of the Slayer");

function scan(item) {
    var fn = arguments.callee;
    bot.monitor(item, function (error) {
        if (error) {
            _utils.logEvent(error);
        }
        fn(item);
    });
}

