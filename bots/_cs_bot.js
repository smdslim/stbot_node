var fs = require("fs"),
    Bot = require("bot"),
    _utils = require("_utils"),
    async = require("async"),
    lotNames = [];

var bot = new Bot(_utils.gamesData.CS.id);
bot.options.edgePercent = 25;

fs.readFile('./lots/cs_regular_lots.json', function (err, data) {
    if (err) throw err;
    lotNames = JSON.parse(data.toString());
    _utils.logEvent("CS_bot started, Lot amount: " + lotNames.length);

    async.map(lotNames, function (item) {
        var fn = arguments.callee;
        bot.monitor(item, function (error) {
            if (error) _utils.logEvent(error);
            fn(item);
        });
    })
})