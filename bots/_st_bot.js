var fs = require("fs"),
    Bot = require("bot"),
    _utils = require("_utils"),
    async = require("async"),
    lotNames = [];

var bot = new Bot(_utils.gamesData.STEAM.id);
bot.options.edgePercent = 30;

fs.readFile('./lots/st_regular_lots.json', function (err, data) {
    if (err) throw err;
    lotNames = JSON.parse(data.toString());
    _utils.logEvent("STEAM_bot started, Lot amount: " + lotNames.length);

    async.map(lotNames, function (item) {
        var fn = arguments.callee;
        bot.monitor(item, function (error) {
            if (error) _utils.logEvent(error);
            fn(item);
        });
    })
})