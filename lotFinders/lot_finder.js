var http = require("http"),
    fs = require("fs"),
    cheerio = require("cheerio"),
    _utils = require("_utils");


var finder = {

    HOSTNAME: "steamcommunity.com",
    PORT: 80,
    acceptableLots: [],

    sessionData: _utils.getConfig().sessionData,

    getCookieString: function () {
        var str = ""
        for (var key in this.sessionData) {
            str += key + "=" + this.sessionData[key] + "; "
        }
        return str;
    },

    scan: function (num) {
        var _this = this;

        this.checkForOptions();

        num = (num === undefined) ? 0 : num;
        console.log("iteration: " + num);
        this.options.path = "/market/search/render/?query=appid%3A" + this.options.appID + "&start=" + num + "&count=" + this.options.perPage;
        var params = {
                hostname: this.HOSTNAME,
                port: this.PORT,
                path: this.options.path,
                method: 'GET',
                headers: {
                    Cookie: this.getCookieString()
                }
            },
            request = http.request(params, function (res) {
                var data = "";
                res.on("data", function (chunk) {
                    data += chunk.toString();
                });
                res.on("end", function () {
                    var tempLots;
                    try {
                        data = unescape(JSON.parse(data).results_html);
                        tempLots = _this.parseData(data);
                        if (tempLots.length > 0) {
                            _this.acceptableLots = _this.acceptableLots.concat(tempLots);
                        }
                    } catch (e) {
                        console.log("error parsing json", e.message);
                        return 0;
                    }
                    if (num < _this.options.elementsAmountAvailable) {
                        _this.scan(num + 100);
                    } else {
                        _this.acceptableLots = _this.acceptableLots.filter(_this.unique);
                        console.log("amount: " + _this.acceptableLots.length);
                        fs.writeFileSync("./lots/" + _this.options.prefix + "regular_lots.json", JSON.stringify(_this.acceptableLots));
                    }
                })
            });
        request.on("close", function (e) {
            console.log("closed");
        });
        request.on('error', function (e) {
            console.log(e.message);
        });
        request.end();
    },

    checkForOptions: function () {
        if (this.options === undefined) {

            throw new Error("No options found, please set them up first!");

        } else if (this.options.appID === undefined || this.options.minPrice === undefined ||
            this.options.maxPrice === undefined || this.options.minAmount === undefined ||
            this.options.perPage === undefined || this.options.prefix === undefined || this.options.elementsAmountAvailable === undefined) {

            throw new Error("Some options are missing, check it");
        }
    },

    parseData: function (html) {
        var $ = cheerio.load(html),
            str,
            price,
            name,
            lots = [],
            amount,
            _this = this;

        $(".market_listing_num_listings").each(function () {
            if ($(this)[0].children.length == 3) {
                str = $(this).text().trim();
                try {
                    price = parseInt(parseFloat(/От\r\n\t\t\t\t(.+)\sp/.exec(str)[1].replace(",", ".")) * 1000 / 10);
                    name = new RegExp(_this.options.appID + "\/(.+)$").exec($(this).closest(".market_listing_row_link").attr("href"))[1];
                    amount = parseInt($(this).find(".market_listing_num_listings_qty").text().trim().replace(",", ""));
                    if (price >= _this.options.minPrice && amount >= _this.options.minAmount && price <= _this.options.maxPrice) {
                        lots.push(name);
                    }
                } catch (e) {
                    console.log("error parsing price/name", e.message);
                }
            }
        })
        return lots;
    },

    unique: function (value, index, self) {
        return self.indexOf(value) === index;
    }
}

module.exports = finder;