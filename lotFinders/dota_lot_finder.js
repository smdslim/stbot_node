var finder = require("./lot_finder");

var options = {
    appID: 570,
    minPrice: 80,
    maxPrice: 1000,
    minAmount: 30,
    perPage: 100,
    prefix: "dota_",
    elementsAmountAvailable: 6223
};

finder.options = options;

finder.scan();