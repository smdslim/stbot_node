var finder = require("./lot_finder");

var options = {
    appID: 753,
    minPrice: 300,
    maxPrice: 400,
    minAmount: 30,
    perPage: 100,
    prefix: "st_",
    elementsAmountAvailable: 10500
};

finder.options = options;

finder.scan();