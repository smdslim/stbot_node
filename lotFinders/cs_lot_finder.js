var finder = require("./lot_finder");

var options = {
    appID: 730,
    minPrice: 100,
    maxPrice: 5000,
    minAmount: 30,
    perPage: 100,
    prefix: "cs_",
    elementsAmountAvailable: 1700
};

finder.options = options;

finder.scan();
