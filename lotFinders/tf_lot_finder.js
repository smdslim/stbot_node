var finder = require("./lot_finder");

var options = {
    appID: 440,
    minPrice: 100,
    maxPrice: 5000,
    minAmount: 50,
    perPage: 100,
    prefix: "tf_",
    elementsAmountAvailable: 2500
};

finder.options = options;

finder.scan();
