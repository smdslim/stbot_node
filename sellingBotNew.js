var _utils = require("_utils"),
    bot = require("./sb"),
    lastReexposeStamp,
    now;


var goNext = function () {
    lastReexposeStamp = _utils.getLastExposeDate();
    now = Date.now();
    bot.begin(function (err, res) {
        if (!err) {
            if (now - lastReexposeStamp >= 3600 * 1000) {
                bot.startReExposing(function () {
                    _utils.logEvent("Done reexposing");
                    _utils.setLastExposeDate(Date.now());
                    goNext();
                });
            } else {
                setTimeout(function () {
                    goNext();
                }, 10000)
            }
        } else {
//            If something goes wrong - reload session and refresh market page - and continue iteration
            _utils.logEvent(err);
            _utils.logEvent("Lets restart");
            bot.driver.get("http://steamcommunity.com/market").then(function () {
                bot.setUpSession();
                bot.navigation.refresh().then(goNext);
            })
        }
    });
}

bot.init(function () {
    _utils.logEvent("Bot initiated");
    goNext();
})







